<?php

class CMDirectoryPageFormExtension extends DataExtension
{

    private static $has_one = array(
        'ContactForm' => 'UserDefinedForm'
    );
    
    public function updateCMSFields(FieldList $fields)
    {
        $fields->findOrMakeTab('Root.Forms', _t('CMDirectoryPageFormExtension.FormsTab', 'Forms'));
        
        $contactForms = UserDefinedForm::get()->map('ID','MenuTitle');
		$formName = Injector::inst()->create('UserDefinedForm')->i18n_singular_name();
		$fields->addFieldToTab(
			'Root.Forms',
			DropdownField::create(
				'ContactFormID',
				_t('CMDirectoryPageFormExtension.ContactFormField','Contact form page'),
				$contactForms
			)->setEmptyString(_t('CMDirectoryPageFormExtension.SelectContactFormPlaceholder','Select')));

    }
   
}
