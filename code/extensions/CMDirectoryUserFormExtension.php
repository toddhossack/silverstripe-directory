<?php

/**
 * Allows sending emails from directory entries via UserForms
 */
class CMDirectoryUserFormExtension extends DataExtension
{

    private static $has_many = array(
        'DirectoryPages' => 'CMDirectoryPage'
    );
    
    public function updateFormFields(FieldList $fields)
    {
        //var_dump($fields);
        $entry = $this->findEntry($_REQUEST);
        
        if(!is_object($entry)) {
                return;
        } elseif(!empty($entry->EmailName) && !empty($entry->Email)) {
            // Display intended recipient
            $recipientField = LiteralField::create('RecipientName',
                _t('CMDirectoryUserFormExtension.RecipientName','<p class="cmdirectoryuserform__recipient">This form submission will be sent to: <strong>{entryEmailName}</strong></p>','Displays recipient name, based on the specified entry', ['entryEmailName' => $entry->EmailName]));
            $fields->unshift($recipientField);
            // Add hidden field to pass on EntryID
            $fields->unshift(HiddenField::create('EntryID',null,$entry->ID));
    
        }
    }
    
    public function findEntry($data)
    {
        if(!empty($data['EntryID'])) {
            $entryID = intval($data['EntryID']);
            return CMDirectoryBasicEntry::get_by_id('CMDirectoryBasicEntry',$entryID);
        } else {
            return null;
        }
            
    }
    
}
