<?php

class CMDirectoryUserDefinedFormExtension extends DataExtension
{
    
    public function updateFilteredEmailRecipients($recipients, $data, $form)
    {
        $entry = $form->findEntry($data);
        if(!is_object($entry)) {
            return;
        }

        // Find default recipient
        $default = $recipients->find('DirectoryDefaultRecipient',1);

        // No default recipient to customise, so create one
        if(!$default) {
            $recipient = $this->createRecipient($entry,$form);
            $recipients->push($recipient);
        }
        // Have default recipient
        else {
            // Entry email available. Clone default and customise with entry email.
            if(!empty($entry->Email)) {
                $recipient = $default->duplicate(false);
                $recipient->EmailAddress = $entry->Email;
                $recipients->push($recipient);
            } 
            // No entry email. Send to default address with ATTN to entry name.
            else {
                $recipient = $default;
                $attn = '<p>ATTN: <strong>'. $entry->FullName .'</strong></p>';
                $recipient->EmailBodyHtml = $attn . $recipient->EmailBodyHtml;
            }
        }
        
        // Remove extraneous recipients 
        foreach($recipients->getIterator() as $r) {
            if($r->ID !== $recipient->ID) {
                $recipients->remove($r);
            }
        }
    }
    
    
    protected function createRecipient($entry,$form)
    {
        $recipient = UserDefinedForm_EmailRecipient::create([
            'FormID' => $form->ID,
            'EmailAddress' => $entry->Email,
            'EmailSubject' => _t('CMDirectoryPageFormExtension.DefaultEmailSubject','Directory Contact Form Submission'),
            'EmailTemplate' => 'SubmittedFormEmail'
        ]);
        return $recipient;
    }
    
    public function updateEmailData($emailData, $attachments)
    {
        
    }
    
    public function updateEmail($email, $recipient, $emailData)
    {
        
    }
    
}
