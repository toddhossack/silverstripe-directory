<?php

class CMDirectoryPageFormControllerExtension extends Extension
{

    protected $_cached = [];
    
    public function EntryContactLink($entryID)
    {
        // Check entry ID
        if(empty($entryID)) {
            return '';
        }
        // Find contact form
        if(!isset($this->_cached['ContactFormPage'])) {
            $this->_cached['ContactFormPage'] = $this->owner->ContactForm() ?: false;
        }

        if(is_object($this->_cached['ContactFormPage'])) {     
            return Controller::join_links($this->_cached['ContactFormPage']->Link() .'?EntryID='.intval($entryID));
        } else {
            return '';
        }
    }

}
