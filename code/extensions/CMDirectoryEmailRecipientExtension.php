<?php

class CMDirectoryEmailRecipientExtension extends DataExtension
{

    private static $db = array(
        'DirectoryDefaultRecipient' => 'Boolean'
    );
    
    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab('Root.EmailDetails',array(
            FieldGroup::create(
                CheckboxField::create('DirectoryDefaultRecipient',
				_t('CMDirectoryEmailRecipientExtension.DirectoryDefaultRecipient','Default Recipient')
                )
            )
            ->setTitle(_t('CMDirectoryEmailRecipientExtension.DirectoryIntegration', 'Directory Integration'))
            ->setDescription(_t(
                'CMDirectoryEmailRecipientExtension.DirectoryIntegration',
                'Default recipient for directory contact submissions'
            ))
        ));
         
    }
   
}
