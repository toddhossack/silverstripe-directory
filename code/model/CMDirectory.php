<?php

class CMDirectory extends DataObject implements PermissionProvider
{

	private static $db = array(
        'Name' => 'Varchar(255)',
		'EntryTypes' => 'Text'
    );

    private static $has_many = array(
        'Categories' => 'CMDirectoryCategory'
    );
	
	private static $many_many = array(
		'Entries' => 'CMDirectoryEntry'
    );

	private static $belongs_to = array(
       'Page' => 'CMDirectoryPage'
    );
	
    protected $_cached = [];    // Instance cache
    
    /**
     * Restrict
     * @config
     * @var type 
     */
    //private static $multiple_entry_types = true;
    
    /* 
	 * -------------------------------------------------------------------------
	 *  Admin methods
	 * -------------------------------------------------------------------------
	 */
	/**
	 * @config
	 * @var array 
	 
	private static $entry_types = array(
		'CMDirectoryPersonEntry',
		'CMDirectoryBusinessEntry'
	);
	*/
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

		$fields->addFieldToTab('Root.Main',TextField::create('Name',_t('CMDirectory.Name','Name')));
		
		/*
		 * Entry type options
		 */
		$entryClasses = ClassInfo::subClassesFor('CMDirectoryEntry');
		array_shift($entryClasses);
        
        //$multipleTypes = (bool) CMDirectory::config()->get('multiple_entry_types');
		$entryTypes = array();
		foreach($entryClasses as $className) {
			try {
				$inst = Injector::inst()->create($className);
				$entryTypes[$className] = $inst->i18n_singular_name();
			} catch (Exception $ex) {
				error_log("$className could not be created. ". $ex->getMessage());
			}
		}
        /*
        // Restrict selection according to config
        if(!$multipleTypes && strpos($this->EntryTypes,',') !== false) {
            $entries = explode(',',$this->EntryTypes);
            $this->EntryTypes = array_shift($entries);
        }
        */
		$fields->addFieldToTab('Root.Main', ListBoxField::create(
			'EntryTypes',
			_t('CMDirectory.EntryTypes','Entry types'),
			$entryTypes,
			'',
			null,
			true	// multiple?
		));
		
		/**
		 * Categories
		 */
		$categoriesGrid = $fields->dataFieldByName('Categories');

		if ($categoriesGrid) {
			// Remove relation link autocompleter
			$autoCompleter = $categoriesGrid->getConfig()->getComponentByType('GridFieldAddExistingAutocompleter');
			$categoriesGrid->getConfig()->removeComponent($autoCompleter);

			// Sorting
            if (class_exists('GridFieldOrderableRows')) {
                $categoriesGrid->getConfig()->addComponent(new GridFieldOrderableRows('Sort'));
            } elseif (class_exists('GridFieldSortableRows')) {
                $categoriesGrid->getConfig()->addComponent(new GridFieldSortableRows('Sort'));
            }
		}
		
		/**
		 * Entries
		 * @todo Allow both unlinking and deleting
		 */
		$fields->removeByName('Entries');
		// Only display entries after saving Directory, so we have the entry types
		if($this->exists()) {
			// Modify Entries GridField config
			$entryGridConf = GridFieldConfig_RelationEditor::create();
			$entryGridConf->removeComponentsByType('GridFieldAddNewButton')->addComponent(Injector::inst()->get('GridFieldAddNewMultiClass'));
			
			$selectedEntryTypes = explode(',',$this->EntryTypes);

            
			// If only one entry type, set as default
			$defaultEntryType = (is_array($selectedEntryTypes) && count($selectedEntryTypes) === 1) ? current($selectedEntryTypes): null;

			$entryGridConf->getComponentByType('GridFieldAddNewMultiClass')->setClasses($selectedEntryTypes,$defaultEntryType);
			$entriesPlural = CMDirectoryEntry::create()->i18n_plural_name();
            
			// Remove relation link autocompleter
			$autoCompleter = $entryGridConf->getComponentByType('GridFieldAddExistingAutocompleter')
                ->setResultsFormat('$FullName ($Email)')->setSearchFields(array('FullName'));
 
            $autoCompleter->setSearchList(CMDirectoryEntry::get());
            
			//$entryGridConf->removeComponent($autoCompleter);
			
			// Replace delete action and specify actual deleting, not just removing
            $delete = $entryGridConf->getComponentByType('GridFieldDeleteAction');
            $entryGridConf->removeComponent($delete);
            $entryGridConf->addComponent(new GridFieldDeleteOrUnlinkAction(false));

			$fields->addFieldToTab("Root.$entriesPlural", GridField::create('Entries',$entriesPlural, $this->Entries(), $entryGridConf));
		
		}
		
        return $fields;
    }
    
    /*
    public function Link() {
        return $this->Directory()->Link('browse/'.$this->ID);
    }
     * 
     */
	
    /* 
	 * -------------------------------------------------------------------------
	 *  Permission methods
	 * -------------------------------------------------------------------------
	 */
    
    public function canView($member = null)
    {
        return true;
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMDIRECTORY_EDIT');
    }

    public function canDelete($member = null)
    {
        return Permission::check('CMDIRECTORY_DELETE');
    }

    public function canCreate($member = null)
    {
        return Permission::check('CMDIRECTORY_CREATE');
    }
	
	public function providePermissions()
    {
        return array(
			'CMDIRECTORY_CREATE' => array(
                'name' => _t(
                    'CMDirectory.CreatePermissionLabel',
                    'Create a directory'
                ),
                'category' => _t(
                    'CMDirectory.PermissionCategory',
                    'Directories'
                ),
            ),
            'CMDIRECTORY_EDIT' => array(
                'name' => _t(
                    'CMDirectory.EditPermissionLabel',
                    'Edit a directory'
                ),
                'category' => _t(
                    'CMDirectory.PermissionCategory',
                    'Directories'
                ),
            ),
            'CMDIRECTORY_DELETE' => array(
                'name' => _t(
                    'CMDirectory.DeletePermissionLabel',
                    'Delete a directory and all its categories'
                ),
                'category' => _t(
                    'CMDirectory.PermissionCategory',
                    'Directories'
                ),
            )
        );
    }
    
    /* 
	 * -------------------------------------------------------------------------
	 *  Data methods
	 * -------------------------------------------------------------------------
	 */
    
    /**
     * 
     * @return ArrayList
     */
    public function EnabledCategories()
    {
        if(!isset($this->_cached['EnabledCategories'])) {
            $category = CMDirectoryCategory::create();
            if($category->hasField('Disabled')) {
                $this->_cached['EnabledCategories'] = $this->Categories()->filter([
                    'Disabled' => 0 
                ]);
            } else {
                $this->_cached['EnabledCategories'] = $this->Categories();
            }
        }
        return $this->_cached['EnabledCategories'];
    }
    
    public function EnabledChildCategories()
    {
        if(!isset($this->_cached['EnabledChildCategories'])) {
            $category = CMDirectoryCategory::create();
            $filters = ['ParentID' => 0];
            if($category->hasField('Disabled')) {
                $filters['Disabled'] = 0;  
            }
            $this->_cached['EnabledChildCategories'] = $this->Categories()->filter($filters);
        }
        return $this->_cached['EnabledChildCategories'];
    }
    
    public function ChildCategories()
    {
        return $this->Categories()->filter([
           'ParentID' => 0 
        ]);
    }
    
    public function ChildCategoriesMap()
    {
        return $this->Categories()->filter([
           'ParentID' => 0 
        ])->map('ID','Name');
    }
    
    
    public function EnabledEntries()
    {
        if(!isset($this->_cached['EnabledEntries'])) {
            $entry = CMDirectoryEntry::create();
            if($entry->hasField('Disabled')) {
                $this->_cached['EnabledEntries'] = $this->Entries()->filter([
                    'Disabled' => 0 
                ]);
            } else {
                $this->_cached['EnabledEntries'] = $this->Entries();
            }
        }
        return $this->_cached['EnabledEntries'];
    }
    
}
