<?php

class CMDirectoryBasicEntry extends CMDirectoryEntry
{
    
    private static $db = array(
        'Phone' => 'Varchar(30)',
        'Mobile' => 'Varchar(30)',
        'Email' => 'Varchar(100)',
        'Website' => 'Varchar',
        
        // Physical address
        'AddressLine1' => 'Varchar',
		'AddressLine2' => 'Varchar',
        'Suburb' => 'Varchar',
        'City' => 'Varchar',
        'State' => 'Varchar',
        'Country' => 'Varchar(2)',
        
        // Mailing address
        'MailingAddressLine1' => 'Varchar',
		'MailingAddressLine2' => 'Varchar',
        'MailingSuburb' => 'Varchar',
        'MailingCity' => 'Varchar',
        'MailingState' => 'Varchar',
        'MailingCountry' => 'Varchar(2)',
        'MailingPostCode' => 'Varchar(16)',

    );
    
    private static $searchable_fields = [
        'FullName'
    ];
    
	/**
	 * @config
	 * @var array 
	 */
	private static $disabled_fields = array();
    
    /**
	 * @config
	 * @var boolean 
	 */
	private static $unformatted_phone_input = false;
    
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        
		/* 
		 * Main fields
		 */
        $unformattedPhone = $this->config()->get('unformatted_phone_input');
        
        if($unformattedPhone) {
            $phoneField = TextField::create('Phone',_t('CMDirectoryBasicEntry.Phone','Phone'),null,30);
            $mobileField = TextField::create('Mobile',_t('CMDirectoryBasicEntry.Mobile','Mobile'),null,30);
        } else {
            $phoneField = PhoneNumberField::create('Phone',_t('CMDirectoryBasicEntry.Phone','Phone'),null,'','');
            $mobileField = PhoneNumberField::create('Mobile',_t('CMDirectoryBasicEntry.Mobile','Mobile'),null,'','');
        }

        $fields->addFieldsToTab('Root.Main', array(
			// Basic details
			TextField::create('Name',_t('CMDirectoryBasicEntry.Name','Name')),
            $phoneField,
            $mobileField,
            EmailField::create('Email',_t('CMDirectoryBasicEntry.Email','Email')),
            TextField::create('Website',_t('CMDirectoryBasicEntry.Website','Website'))
        ));
        
        $fields->addFieldsToTab('Root.Main', 
            HeaderField::create('BasicHeading',_t('CMDirectoryBasicEntry.BasicHeading','Basic details'))
        ,'Name');
        
        
        /* 
         * Addresses
         */
        $fields->findOrMakeTab('Root.Address',_t('CMDirectoryBasicEntry.AddressTab','Address'));
		$fields->addFieldsToTab('Root.Address',[
            
            // Physical Address
            HeaderField::create('AddressHeading',_t('CMDirectoryBasicEntry.AddressHeading','Physical address')),
			TextField::create('AddressLine1',_t('CMDirectoryBasicEntry.AddressLine1','Address line 1')),
			TextField::create('AddressLine2',_t('CMDirectoryBasicEntry.AddressLine2','Address line 2')),
			TextField::create('Suburb',_t('CMDirectoryBasicEntry.Suburb','Suburb')),
			TextField::create('City',_t('CMDirectoryBasicEntry.City','City')),
			TextField::create('State',_t('CMDirectoryBasicEntry.State','State')),
			CountryDropdownField::create('Country',_t('CMDirectoryBasicEntry.Country','Country')),
			
            // Mailing adddress
            HeaderField::create('MailingAddressHeading',_t('CMDirectoryBasicEntry.MailingAddressHeading','Mailing address')),
			TextField::create('MailingAddressLine1',_t('CMDirectoryBasicEntry.MailingAddressLine1','Address line 1')),
			TextField::create('MailingAddressLine2',_t('CMDirectoryBasicEntry.MailingAddressLine2','Address line 2')),
			TextField::create('MailingSuburb',_t('CMDirectoryBasicEntry.MailingSuburb','Suburb')),
			TextField::create('MailingCity',_t('CMDirectoryBasicEntry.MailingCity','City')),
			TextField::create('MailingState',_t('CMDirectoryBasicEntry.MailingState','State')),
			CountryDropdownField::create('MailingCountry',_t('CMDirectoryBasicEntry.MailingCountry','Country')),
			TextField::create('MailingPostCode',_t('CMDirectoryBasicEntry.MailingPostCode','PostCode'))
        ]);

		// Remove fields disabled in config
		$this->removeDisabledFields($fields);

        return $fields;
    }
  
    /*
    public function Link() {
        return $this->Directory()->Link('browse/'.$this->ID);
    }
     * 
     */
    
	public function WebsiteLink() {
		$url = $this->Website;
		if(strpos($url,'http') === false) {
			$url = 'http://'. $url;
		}
		return $url;
	}
	
    public function getEmailName() {
        return $this->Name;
    }
    
    public function getSummaryContact()
	{
        $parts = [];
		if(!empty($this->Email)) {
            $parts[] = $this->Email;
        }
        $phone = $this->Phone ?: $this->Mobile;
        if(!empty($phone)) {
            $parts[] = $phone;
        }
        return implode(' / ',$parts);
	}
    
	protected function locale() {
		if (($member = Member::currentUser()) && $member->Locale) return $member->Locale;
		return i18n::get_locale();
	}
}
