<?php

class CMDirectoryPersonEntry extends CMDirectoryBasicEntry 
{
    
	private static $db = array(
        'MiddleName' => 'Varchar',
        'LastName' => 'Varchar',
        'Prefix' => 'Varchar(30)',
        'Suffix' => 'Varchar(30)',
        'Position' => 'Varchar'
    );
	
    private static $searchable_fields = [
        'FullName',
        'Email'
    ];
    
    protected function createFullName()
    {
        $name = trim($this->getField('Name'));
        $lastName = trim($this->LastName);
        $str =  ($lastName) ? $lastName .', '. $name : $name;
		return str_replace('  ',' ',$str);
    }
    
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
		
		$fields->removeFieldsFromTab('Root.Main',array_keys($this->stat('db')));
		
		$nameFields = array(
            TextField::create('Prefix',_t('CMDirectoryPersonEntry.Prefix','Prefix')),
			TextField::create('Name',_t('CMDirectoryPersonEntry.Name','First name')),
			TextField::create('MiddleName',_t('CMDirectoryPersonEntry.MiddleName','Middle name or initial')),
			TextField::create('LastName',_t('CMDirectoryPersonEntry.LastName','Last name')),
            TextField::create('Suffix',_t('CMDirectoryPersonEntry.Suffix','Suffix')),
		);
		$nameGroup = FieldGroup::create(
			_t('CMDirectoryPersonEntry.NameGroup','Name'),
			$nameFields
		);
		$fields->replaceField('Name',$nameGroup);
        
        
        $fields->addFieldToTab('Root.Main',TextField::create('Position',_t('CMDirectoryPersonEntry.Position','Position')),'Phone');
		// Remove fields disabled in config
		$this->removeDisabledFields($fields);
		
        return $fields;
    }
    
    protected function onBeforeWrite() 
    {
		parent::onBeforeWrite();

        if($this->isChanged('Name',DataObject::CHANGE_VALUE) || $this->isChanged('LastName',DataObject::CHANGE_VALUE)) {
            $this->FullName = $this->createFullName();
        }
	}
    
    public function getEmailName() {
        $str = $this->Name;
        if($this->LastName) {
            $str .= ' '. $this->LastName;
        }
        return $str;
    }
}
