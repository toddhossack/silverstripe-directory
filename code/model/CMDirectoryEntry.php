<?php

class CMDirectoryEntry extends DataObject implements PermissionProvider
{
    private static $db = array(
		'Name' => 'Varchar',
        'Description' => 'HTMLText',
        'FullName' => 'Varchar(100)'    // Used for search
	);
	
    private static $many_many = array(
        'Categories' => 'CMDirectoryCategory'
    );
	
    private static $many_many_extraFields = [
        'Categories' => [
          'Sort' => 'Int'
        ]
    ];
    
	private static $belongs_many_many = array(
		'Directories' => 'CMDirectory'
    );
	
	/**
	 * @config
	 */
	private static $summary_fields = array(
		'FullName',
		'SummaryDirectories',
		'SummaryCategories',
        'SummaryContact'
	);
	
	private static $casting = array(
		'SummaryDirectories' => 'Text',
		'SummaryCategories' => 'Text',
        'SummaryContact' => 'Text',
	);

    private static $default_sort = 'FullName ASC';
    
	/**
	 * @config
	 * @var array 
	 */
	private static $disabled_fields = array();
    
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
		
        // Remove FullName field
        $fields->removeByName('FullName');
        
        // Place description on own tab
        $descriptionField = $fields->dataFieldByName('Description');
        $descriptionField->setRows(15);
        
        $fields->removeByName('Description');
        $fields->findOrMakeTab(
			'Root.Description', _t('CMDirectoryEntry.DescriptionTab','Description')
		);
        $fields->addFieldToTab('Root.Description',$descriptionField);
        
		/**
		 * Directories
		 * @todo Determine context. If not editing within directory, show directories field.
		 */
		//$dirField = $fields->dataFieldByName('Directories');
		
		$fields->removeByName('Directories');
		
		/*
		 * Categories
		 * If not editing within directory, hide categories field. If editing
		 * in directory, limit categories to directory's categories.
		*/
		
		$fields->removeByName('Categories');
		
		// Bit of a hack to get the current directory context. 
		$request = Controller::curr()->getRequest();
		$directoryClass = ($request) ? $request->param('ModelClass') : null;
	
        $queryParams = $this->getSourceQueryParams();
        $directoryId = (isset($queryParams['Foreign.ID'])) ? $queryParams['Foreign.ID'] : null;
        
		// Entry has been saved and we're editing under directory context
		if($this->exists() && $directoryClass === 'CMDirectory' && is_numeric($directoryId)) {
			$categoryInst = CMDirectoryCategory::create();
			$catPluralName = $categoryInst->i18n_plural_name();
			$categoriesField = TreeMultiselectField::create('Categories',_t('CMDirectoryEntry.SelectCategories','Select categories'),'CMDirectoryCategory','ID','Name');
            // Filter the categories, leaving only the categories within this directory
			$categoriesField->setFilterFunction(function($cat) use($directoryId) {
				// Compare the category directory to the current directory
				if(!empty($cat->DirectoryID) && !empty($directoryId)) {
					return intval($cat->DirectoryID) === intval($directoryId);
				}
				return false;
			}); 
			$fields->addFieldToTab("Root.$catPluralName",$categoriesField);
		}
		
        return $fields;
    }
    
	/**
	 * Remove fields disabled in config for this class
	 */
	protected function removeDisabledFields($fields)
	{
		$disabled = $this->config()->get('disabled_fields');
		if(is_array($disabled) && count($disabled)) {
			foreach($disabled as $field) {
				$fields->removeByName($field);
			}
		}
	}
	
	public function fieldLabels($includerelations = true)
	{
		
		return array_merge((array) $this->translatedLabels(), parent::fieldLabels($includerelations));
	}
	
	
	protected function translatedLabels() {
		return array(
			'FullName' => _t('CMDirectoryEntry.Name','Name'),
			'SummaryDirectories' => CMDirectory::create()->i18n_plural_name(),
			'SummaryCategories' => CMDirectoryCategory::create()->i18n_plural_name(),
            'SummaryContact' => _t('CMDirectoryEntry.SummaryContact','Contact'),
		);
	}

	public function getSummaryDirectories()
	{
		$names = (array) $this->Directories()->column('Name');
		return implode(',',$names);
	}
	
	public function getSummaryCategories()
	{
		$names = (array) $this->Categories()->column('Name');
		return implode(',',$names);
	}
    
    public function getSummaryContact()
	{
		return '';
	}
	
    /*
    public function Link() {
        return $this->Directory()->Link('browse/'.$this->ID);
    }
     * 
     */
	
	protected function locale() {
		if (($member = Member::currentUser()) && $member->Locale) return $member->Locale;
		return i18n::get_locale();
	}
	
	public function forTemplate()
	{
		$tpl = get_class($this);
		return $this->renderWith($tpl);
	}
 
    protected function createFullName()
    {
        return $this->Name;
    }
    
    
    /* 
	 * -------------------------------------------------------------------------
	 *  Permissions
	 * -------------------------------------------------------------------------
	 */
	
    /**
	 * 
	 * @see DataObject::providePermissions()
	 * @return type
	 */
	public function providePermissions()
    {
        return array(
			'CMDirectoryEntry_MANAGE' => array(
                'name' => _t(
                    'CMDirectoryEntry.ManagePermissionLabel',
                    'Create,edit,delete directory entries'
                ),
                'category' => _t(
                    'CMDirectory.PermissionCategory',
                    'Directories'
                )
            )
        );
    }
    
    /**
	 * 
	 * @param Member $member
	 * @return boolean
	 */
    public function canCreate($member = null)
    {
        $extended = $this->extendedCan(__FUNCTION__, $member);
		if($extended !== null) {
			return $extended;
		}
        if($member && Permission::check('ADMIN', 'any', $member)) {
            return true;
        }
        return Permission::check('CMDirectoryEntry_MANAGE');
    }
    
	/**
	 * 
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null)
    {
		$extended = $this->extendedCan(__FUNCTION__, $member);
		if($extended !== null) {
			return $extended;
		}
        return true;
    }

	/**
	 * 
	 * @param Member $member
	 * @return boolean
	 */
    public function canEdit($member = null)
    {
        $extended = $this->extendedCan(__FUNCTION__, $member);
		if($extended !== null) {
			return $extended;
		}
		if($member && Permission::check('ADMIN', 'any', $member)) {
            return true;
        }
        return Permission::check('CMDirectoryEntry_MANAGE');
    }
    
    /**
	 * 
	 * @param Member $member
	 * @return boolean
	 */
    public function canDelete($member = null)
    {
        // Disallow deletion of entries linked to more than one directory
        $count = $this->Directories()->Count();
        if($count > 1) return false;
        
		$extended = $this->extendedCan(__FUNCTION__, $member);
		if($extended !== null) {
			return $extended;
		}
		if($member && Permission::check('ADMIN', 'any', $member)) {
            return true;
        }
        return Permission::check('CMDirectoryEntry_MANAGE');
    }
    
}
