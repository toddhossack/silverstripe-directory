<?php

class CMDirectoryPage extends Page 
{
    
    private static $description = 'Pages for directory listings';
    
    private static $allowed_children = array('CMDirectoryCategoryPage');

    private static $has_one = array(
       'Directory' => 'CMDirectory',
    );
	
    public function getCMSFields()
    {
		$fields = parent::getCMSFields();

		$directories = CMDirectory::get()->map('ID','Name');
		$directoryName = Injector::inst()->create('CMDirectory')->i18n_singular_name();
		$fields->addFieldToTab(
			'Root.Main',
			DropdownField::create(
				'DirectoryID',
				$directoryName,
				$directories
			)->setEmptyString(_t('CMDirectoryPage.SelectDirectory','Select directory'))
		,'Content');

		return $fields;
    }
}


class CMDirectoryPage_Controller extends Page_Controller 
{ 
    
   /**
    * Allowed actions
    * @var array 
    */
	private static $allowed_actions = array(
		'search', 
		'DirectorySearchForm'
	);

	private static $url_handlers = array(
		'search//$Category/$ChildCategory' => 'search'
	);
	
    protected $_cached = [];    // Instance cache
	/* 
	 * -------------------------------------------------------------------------
	 *  General action methods
	 * -------------------------------------------------------------------------
	 */

	
	public function search(SS_HTTPRequest $request) {
		$response = Injector::inst()
			->create('CMDirectoryController',$this->Directory(), $this->dataRecord, null)
			->index($request);
		if($response instanceof SS_HTTPResponse) {
			return $response;
		}
		return $this->customise(ArrayData::create(array(
			'SearchResults' => $response
		)));
	}

	/* 
	 * -------------------------------------------------------------------------
	 *  Form methods
	 * -------------------------------------------------------------------------
	 */
	
	
	/**
	 * Search form
	 */
	public function DirectorySearchForm() 
	{
		$controller = Injector::inst()
			->create('CMDirectoryController',$this->Directory(), $this->dataRecord, null);
		$controller->setRequest($this->request);
		return $controller->DirectorySearchForm();
	}
	
	
	
	/* 
	 * -------------------------------------------------------------------------
	 *  Template methods
	 * -------------------------------------------------------------------------
	 */
	
	public function DirectoryAction()
	{
		$action = $this->request->param('Action') ?: 'index';
		return $action;
	}
	
    public function PaginatedEntries()
    {
        if(!isset($this->_cached['PaginatedEntries'])) {
            $records = $this->Directory()->Entries();
            $this->_cached['PaginatedEntries'] = new PaginatedList($records,$this->getRequest());
        }
        return $this->_cached['PaginatedEntries'];
    }
    
    public function PageEntries()
    {
        if(!isset($this->_cached['PageEntries'])) {
            $list = ArrayList::create([]);
            foreach($this->PaginatedEntries() as $record) {
                $record->_link = $this->EntryLink($record);
                $list->push($record);
            }
            $this->_cached['PageEntries'] = $list;
        }
        return $this->_cached['PageEntries'];
    }
    
    /**
     * 
     * @return ArrayList
     */
    public function Categories()
    {
        if(!isset($this->_cached['Categories'])) {
            $records = $this->Directory()->Categories();
            $this->_cached['Categories'] = $records;
        }
        return $this->_cached['Categories'];
    }
    
    public function DirectoryCacheKey() 
    {
        $fragments = [
            'Directory',
            $this->ID,
            // identify which objects are in the list and their sort order
            CMDirectoryCategory::get()->max('LastEdited'),
            CMDirectoryCategory::get()->count(),
            CMDirectoryEntry::get()->max('LastEdited'),
            CMDirectoryEntry::get()->count()
        ];
        return implode('-_-', $fragments);
    }
    
	/* 
	 * -------------------------------------------------------------------------
	 *  Helper methods
	 * -------------------------------------------------------------------------
	 */
	
	public function EntryLink($entry)
    {
        if(is_numeric($entry)) {
            $entry = CMDirectoryEntry::get()->where(['ID' => intval($entry)])->first();
        }
        if(!$entry) {
            return '';
        }
        
        $link = Controller::join_links(Director::baseURL(), $this->RelativeLink(), 'show', $entry->ID);

        return $link;
    }
}