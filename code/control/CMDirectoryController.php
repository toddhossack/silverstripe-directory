<?php


class CMDirectoryController extends Controller
{
	
	/**
	 * @config
	 * @var array
	 */
	private static $allowed_actions = array(
		'index',
		'search'
	);
	
	protected $directory;
	
	protected $page;
	
	public function __construct($directory, $page, $parentCategory = null) {
		parent::__construct();
		
		$this->directory = $directory;
		$this->parentCategory = $parentCategory;
		$this->page = $page;

	}

	public function index(SS_HTTPRequest $request)
	{
		$segment = $request->param('Category');
		$nextSegment = $request->param('ChildCategory');

		/*
		 * Delegate according to category segment
		 */
		// Action method requested (non-category)
		if($segment && $this->hasAction($segment)) {
			return $this->handleAction($request,$segment); 
		}

		$category = $this->findCategoryOrFail($segment,$this->parentCategory);
		
		// Child category present
		if($nextSegment) {
			$response = $this->delegateToNestedController($request, $category);			
		}
		// Category requested
		elseif($category) {
			$response = $this->searchByCategory($category);
		}
		// Category assigned
		elseif($this->parentCategory) {
			$response = $this->searchByCategory($this->parentCategory);
		}
		// No action
		else {
			$response = $this->searchAll();
		}

		if($response instanceof DataList) {
			$this->extend('updateSearchResults',$response, $request);
		}
		return $response;

	}
	
	protected function delegateToNestedController(SS_HTTPRequest $request, $category)
	{
		// Category found
		$request->shiftAllParams();
		$request->shift();

		$response = Injector::inst()->create('CMDirectoryController',$this->directory, $this->page, $category)->index($request);
		return $response;
	}
	
	
	protected function searchByCategory($category)
	{
		return $category->Entries();
	}
	
	
	protected function searchAll()
	{

		return $this->directory->Entries();
	}	
	
	/* 
	 * -------------------------------------------------------------------------
	 *  Form methods
	 * -------------------------------------------------------------------------
	 */
	/**
	 * Search form
	 */
	public function DirectorySearchForm() 
	{
		$parentID = ($this->parentCategory) ? (int) $this->parentCategory->ID : 0;
		$category = $this->findCategory($this->request->param('Category'),$parentID);
		$categoryID = ($category) ? (int) $category->ID : $parentID;	// Default to parent
		
		$fields = FieldList::create(
			DropdownField::create('category',_t('CMDirectoryPage.SearchFormCategoryLabel'),$this->categoryOptions(),$categoryID)
        );
		
		$required = RequiredFields::create();

		// Extension hook
		$this->extend('updateSearchFormFields',$fields, $required);
		
		$actions = FieldList::create(
            FormAction::create("doSearch")->setTitle(_t('CMDirectoryPage.SearchFormSubmit','Search'))
		);
		
		$form = Form::create($this, 'DirectorySearchForm', $fields, $actions, $required);

		$action = $this->page->Link();
		if(strpos($action,'DirectorySearchForm') === false) {
			$action = static::join_links($action,'DirectorySearchForm');
		}

		$form->setFormAction($action);
		$form->setFormMethod('POST');
		
		$this->extend('updateSearchForm',$form);
	
        return $form;
	}
	
	/**
	 * @todo check security?
	 * @return string
	 */
	public function Link()
	{
		return $this->request->getURL();
	}
	/**
	 * Get array of top level categories
	 * @return array
	 */
	protected function categoryOptions() {
		if(!$this->directory) {
			return array();
		}
		return $this->directory->Categories()->filter(array('ParentID' => 0))->map('ID','Name');
	}
	
	/**
	 * 
	 * @param array $data
	 * @param object $form
	 * @return SS_HTTPResponse | false - Redirection
	 */
	public function doSearch($data,$form)
	{

		$catId = (!empty($data['category'])) ? (int) $data['category'] : 0;
		$category = ($catId) ? CMDirectoryCategory::get()->byID($catId) : null;

		$url = $category->Link(true);
	
		return $this->redirect($url);

	}
	
	
	/* 
	 * -------------------------------------------------------------------------
	 *  Helper methods
	 * -------------------------------------------------------------------------
	 */
	
	protected function findCategory($segment,$parentCategory)
	{
		$parentID = ($parentCategory && !empty($parentCategory->ID)) 
			? (int) $parentCategory->ID : 0;
		return CMDirectoryCategory::create()->findBySegment($segment,$this->directory->ID,$parentID);
	}
	
	protected function findCategoryOrFail($segment,$parentCategory)
	{
		$category = $this->findCategory($segment,$parentCategory);

		return ($category) ? $category : $this->handleError(404,
			sprintf(
			_t('CMDirectoryController.CategoryNotFound','Category "%s" not found'),
			$segment)
		);
		
	}
	
	
	/* 
	 * -------------------------------------------------------------------------
	 *  Error methods
	 * -------------------------------------------------------------------------
	 */
	
	protected function handleError($statusCode,$msg)
	{
		$response = class_exists('ErrorPage') ? ErrorPage::response_for($statusCode) : new SS_HTTPResponse($msg, $statusCode);
		// Log server errors
		if(intval($statusCode) >= 500) {
			SS_Log::log($msg,SS_Log::ERR);
		}
		return $this->httpError($statusCode, $response ? $response : $msg);
	}
	
}